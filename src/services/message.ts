import { ApplicationError } from '@enums/application-error';
import { RuntimeException } from '@exceptions/runtime';
import { IMessage } from '@interfaces/message';
import { Injectable, Logger } from '@nestjs/common';
import { MessageRepository } from '@repositories/message';
import { MessageDocument } from '@schemas/message';
import { Role } from '@schemas/role';

@Injectable()
export class MessageService {
  private readonly logger: Logger = new Logger(MessageService.name);

  constructor(private readonly repository: MessageRepository) {}

  async findAll(guild: string): Promise<IMessage[]> {
    const reactions: MessageDocument[] = await this.repository.findByGuild(
      guild,
    );
    return reactions.map((r) => r.toJSON() as IMessage);
  }

  async findOneByGuildAndMessage(
    guild: string,
    message: string,
  ): Promise<IMessage> {
    const reaction: MessageDocument =
      await this.repository.findOneByGuildAndMessage(guild, message);
    return reaction.toJSON() as IMessage;
  }

  async save(data: IMessage): Promise<IMessage> {
    const reaction: MessageDocument = await this.repository.save(data);
    return reaction.toJSON() as IMessage;
  }

  async addReaction(
    guild: string,
    message: string,
    emoji: string,
    role: string,
  ): Promise<IMessage> {
    let reaction: MessageDocument =
      await this.repository.findOneByGuildAndMessage(guild, message);

    if (!reaction) {
      return this.save({
        guild,
        message,
        customContent: null,
        roles: [{ emoji, role }],
      } as any);
    }

    const roles: Role[] = reaction.roles;

    const currentRole = roles.find((x) => x.role === role || x.emoji === emoji);

    if (currentRole) {
      throw new RuntimeException(ApplicationError.REACTION_ALREADY_EXISTS);
    }

    const updatedReaction = await this.repository.addReactionByGuildAndMessage(
      guild,
      message,
      emoji,
      role,
    );

    return updatedReaction.toJSON() as IMessage;
  }

  async removeReaction(
    guild: string,
    message: string,
    emoji: string,
  ): Promise<IMessage> {
    const reaction: MessageDocument =
      await this.repository.findOneByGuildAndMessage(guild, message);

    if (!reaction) {
      throw new RuntimeException(ApplicationError.REACTION_NOT_FOUND);
    }

    const roles: Role[] = reaction.roles;

    const currentRole = roles.find((x) => x.emoji === emoji);

    if (!currentRole) {
      throw new RuntimeException(ApplicationError.REACTION_NOT_FOUND);
    }

    const updatedReaction =
      await this.repository.removeReactionByGuildAndMessageAndEmoji(
        guild,
        message,
        emoji,
      );

    return updatedReaction.toJSON() as IMessage;
  }

  async disableMessage(guild: string, message: string): Promise<boolean> {
    const reaction: MessageDocument =
      await this.repository.findOneByGuildAndMessage(guild, message);

    this.logger.log(reaction);

    if (!reaction) {
      throw new RuntimeException(ApplicationError.REACTION_NOT_FOUND);
    }

    await this.repository.removeMessageByGuildAndMessage(guild, message);

    return true;
  }

  async enableMessage(guild: string, message: string): Promise<IMessage> {
    const reaction: MessageDocument =
      await this.repository.findOneByGuildAndMessage(guild, message);

    if (reaction) {
      throw new RuntimeException(ApplicationError.REACTION_ALREADY_EXISTS);
    }

    const document: MessageDocument = await this.repository.save({
      guild,
      message,
      customContent: null,
      roles: [],
    });

    return document.toJSON() as IMessage;
  }
}
