export enum ApplicationError {
  REACTION_ALREADY_EXISTS = 'reaction-already-exists',
  REACTION_NOT_FOUND = 'reaction-not-found',
}
