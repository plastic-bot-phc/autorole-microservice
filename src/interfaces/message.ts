import { IRole } from '@interfaces/role';

export interface IMessage {
  _id: string;
  guild: string;
  message: string;
  customContent: string;
  roles: IRole[];
}
