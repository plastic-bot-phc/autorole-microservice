import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Role, RoleSchema } from '@schemas/role';
import { Document } from 'mongoose';

export type MessageDocument = Message & Document;

@Schema({
  collection: 'messages',
  timestamps: true,
})
export class Message {
  @Prop({
    required: true,
  })
  guild: string;

  @Prop({
    required: true,
    unique: true,
  })
  message: string;

  @Prop()
  customContent: string;

  @Prop({
    type: [RoleSchema],
  })
  roles: Role[];
}

export const MessageSchema = SchemaFactory.createForClass(Message);
