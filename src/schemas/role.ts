import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  _id: false,
  timestamps: true,
})
export class Role {
  @Prop({
    required: true,
  })
  role: string;

  @Prop({
    required: true,
  })
  emoji: string;
}

export const RoleSchema = SchemaFactory.createForClass(Role);
