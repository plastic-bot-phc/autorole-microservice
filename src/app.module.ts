import { MONGO_URI_ENV } from '@constants/environment';
import { AutoRoleCommandController } from '@controllers/autorole-command';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { MessageRepository } from '@repositories/message';
import { Message, MessageSchema } from '@schemas/message';
import { MessageService } from '@services/message';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get<string>(MONGO_URI_ENV),
      }),
    }),
    MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }]),
  ],
  controllers: [AutoRoleCommandController],
  providers: [MessageService, MessageRepository],
})
export class AppModule {}
