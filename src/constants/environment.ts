export const MONGO_URI_ENV = 'MONGO_URI';

export const REDIS_URI_ENV = 'REDIS_URI';

export const REDIS_PASSWORD_ENV = 'REDIS_PASSWORD';
