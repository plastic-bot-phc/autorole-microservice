import {
  AUTOROLE_ADD_REACTION_PATTERN,
  AUTOROLE_MESSAGE_ALL_PATTERN,
  AUTOROLE_MESSAGE_DISABLE_PATTERN,
  AUTOROLE_MESSAGE_ENABLE_PATTERN,
  AUTOROLE_MESSAGE_ONE_PATTERN,
  AUTOROLE_REMOVE_REACTION_PATTERN,
} from '@constants/pattern';
import { AddReactionDTO } from '@dtos/add-reaction';
import { DisableMessageDTO } from '@dtos/disable-message';
import { EnableMessageDTO } from '@dtos/enable-message';
import { GetReactionDTO } from '@dtos/get-reaction';
import { GetReactionsDTO } from '@dtos/get-reactions';
import { RemoveReactionDTO } from '@dtos/remove-reaction';
import { IMessage } from '@interfaces/message';
import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { MessageService } from '@services/message';

@Controller()
export class AutoRoleCommandController {
  private readonly logger: Logger = new Logger(AutoRoleCommandController.name);

  constructor(private readonly messageService: MessageService) {}

  @MessagePattern(AUTOROLE_MESSAGE_ALL_PATTERN)
  getReactions(@Payload() data: GetReactionsDTO): Promise<IMessage[]> {
    return this.messageService.findAll(data.guild);
  }

  @MessagePattern(AUTOROLE_MESSAGE_ONE_PATTERN)
  getReaction(@Payload() data: GetReactionDTO): Promise<IMessage> {
    return this.messageService.findOneByGuildAndMessage(
      data.guild,
      data.message,
    );
  }

  @MessagePattern(AUTOROLE_MESSAGE_ENABLE_PATTERN)
  enableMessage(@Payload() data: EnableMessageDTO): Promise<IMessage> {
    return this.messageService.enableMessage(data.guild, data.message);
  }

  @MessagePattern(AUTOROLE_MESSAGE_DISABLE_PATTERN)
  disableMessage(@Payload() data: DisableMessageDTO): Promise<boolean> {
    return this.messageService.disableMessage(data.guild, data.message);
  }

  @MessagePattern(AUTOROLE_ADD_REACTION_PATTERN)
  addReaction(@Payload() data: AddReactionDTO): Promise<IMessage> {
    return this.messageService.addReaction(
      data.guild,
      data.message,
      data.emoji,
      data.role,
    );
  }

  @MessagePattern(AUTOROLE_REMOVE_REACTION_PATTERN)
  removeReaction(@Payload() data: RemoveReactionDTO): Promise<IMessage> {
    return this.messageService.removeReaction(
      data.guild,
      data.message,
      data.emoji,
    );
  }
}
