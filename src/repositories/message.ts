import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Message, MessageDocument } from '@schemas/message';
import { Model } from 'mongoose';
@Injectable()
export class MessageRepository {
  constructor(
    @InjectModel(Message.name)
    private readonly model: Model<Message>,
  ) {}

  findByGuild(guild: string): Promise<MessageDocument[]> {
    return this.model.find({ guild }).exec();
  }

  save(data: Message): Promise<MessageDocument> {
    return new this.model(data).save();
  }

  findOneByGuildAndMessage(
    guild: string,
    message: string,
  ): Promise<MessageDocument> {
    return this.model.findOne({ guild, message }).exec();
  }

  addReactionByGuildAndMessage(
    guild: string,
    message: string,
    emoji: string,
    role: string,
  ): Promise<MessageDocument> {
    return this.model
      .findOneAndUpdate(
        { guild, message },
        {
          $push: { roles: { emoji, role } },
        },
        {
          new: true,
        },
      )
      .exec();
  }

  removeReactionByGuildAndMessageAndEmoji(
    guild: string,
    message: string,
    emoji: string,
  ): Promise<MessageDocument> {
    return this.model
      .findOneAndUpdate(
        { guild, message },
        {
          $pull: { roles: { emoji } },
        },
        {
          new: true,
        },
      )
      .exec();
  }

  async removeMessageByGuildAndMessage(
    guild: string,
    message: string,
  ): Promise<boolean> {
    await this.model.findOneAndDelete({ guild, message }).exec();
    return true;
  }
}
