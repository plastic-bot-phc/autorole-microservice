export class DisableMessageDTO {
  guild: string;
  message: string;
}
