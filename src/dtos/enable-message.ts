export class EnableMessageDTO {
  guild: string;
  message: string;
}
