export class AddReactionDTO {
  guild: string;
  message: string;
  emoji: string;
  role: string;
}
