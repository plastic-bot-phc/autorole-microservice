export class RemoveReactionDTO {
  guild: string;
  message: string;
  emoji: string;
}
